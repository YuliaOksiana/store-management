Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  # api/v1/tokos

  namespace :api do
    namespace :v1 do
      resources :tokos
      resources :kasir
      resources :pembeli
      resources :produk
      resources :transaksi
      resources :detail_transaksi
      # get '/detailtransaksi', to: 'DetailTransaksis#index'
    end
  end
end
