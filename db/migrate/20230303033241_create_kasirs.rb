class CreateKasirs < ActiveRecord::Migration[7.0]
  def change
    create_table :kasirs do |t|
      t.integer :id_kasir
      t.string :nama_kasir
      t.string :nama_barang
      t.integer :harga_barang
      t.integer :total_barang
      t.integer :total_harga
      t.integer :total_diskon
      t.integer :id_toko

      t.timestamps
    end
  end
end
