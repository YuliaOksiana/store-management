class CreatePembelis < ActiveRecord::Migration[7.0]
  def change
    create_table :pembelis do |t|
      t.integer :id_pembeli
      t.date :tgl_transaksi
      t.integer :total_barang
      t.integer :total_harga
      t.integer :total_diskon
      t.string :metode_bayar
      t.timestamps
    end
  end
end
