class CreateDetailTransaksis < ActiveRecord::Migration[7.0]
  def change
    create_table :detail_transaksis do |t|
      t.integer :id_detail
      t.integer :id_barang
      t.string :nama_barang
      t.integer  :harga_barang
      t.date :tgl_transaksi
      t.integer :total_barang
      t.integer :total_harga
      
      t.timestamps
    end
  end
end
