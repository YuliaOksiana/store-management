class CreateTransaksis < ActiveRecord::Migration[7.0]
  def change
    create_table :transaksis do |t|
      t.integer :id_transaksi
      t.integer :id_kasir
      t.date :tgl_transaksi
      t.string :nama_barang
      t.integer :harga_barang
      t.integer :total_barang
      t.integer :total_harga
      t.integer :total_diskon
      t.string :metode_bayar
      t.timestamps
    end
  end
end
