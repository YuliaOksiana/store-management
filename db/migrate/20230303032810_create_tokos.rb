class CreateTokos < ActiveRecord::Migration[7.0]
  def change
    create_table :tokos do |t|
      t.integer :id_toko
      t.string :nama_toko
      t.integer :no_telp
      t.string :alamat_toko

      t.timestamps
    end
  end
end
