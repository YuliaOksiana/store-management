class CreateProduks < ActiveRecord::Migration[7.0]
  def change
    create_table :produks do |t|
      t.integer :id_barang
      t.string :nama_barang
      t.string :kategori
      t.integer :harga_beli
      t.integer :harga_jual
      
      t.timestamps
    end
  end
end
