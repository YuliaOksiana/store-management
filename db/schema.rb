# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_03_03_033636) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "detail_transaksis", force: :cascade do |t|
    t.integer "id_detail"
    t.integer "id_barang"
    t.string "nama_barang"
    t.integer "harga_barang"
    t.date "tgl_transaksi"
    t.integer "total_barang"
    t.integer "total_harga"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kasirs", force: :cascade do |t|
    t.integer "id_kasir"
    t.string "nama_kasir"
    t.string "nama_barang"
    t.integer "harga_barang"
    t.integer "total_barang"
    t.integer "total_harga"
    t.integer "total_diskon"
    t.integer "id_toko"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pembelis", force: :cascade do |t|
    t.integer "id_pembeli"
    t.integer "id_barang"
    t.string "nama_barang"
    t.date "tgl_transaksi"
    t.integer "total_barang"
    t.integer "total_harga"
    t.integer "total_diskon"
    t.string "metode_bayar"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "produks", force: :cascade do |t|
    t.integer "id_barang"
    t.string "nama_barang"
    t.string "kategori"
    t.integer "harga_beli"
    t.integer "harga_jual"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tokos", force: :cascade do |t|
    t.integer "id_toko"
    t.string "nama_toko"
    t.integer "no_telp"
    t.string "alamat_toko"
    t.integer "id_barang"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transaksis", force: :cascade do |t|
    t.integer "id_transaksi"
    t.integer "id_kasir"
    t.date "tgl_transaksi"
    t.string "nama_barang"
    t.integer "harga_barang"
    t.integer "total_barang"
    t.integer "total_harga"
    t.integer "total_diskon"
    t.string "metode_bayar"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
