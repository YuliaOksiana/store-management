class Api::V1::TokosController < ApplicationController
    before_action :set_toko, only: [:show, :update, :destroy]

    # GET /toko
    def index
      @tokos = Toko.all
  
      render json: @tokos.map { |toko| toko.new_attributes }
    end
  
    # GET /toko/1
    def show
      render json: @toko.new_attributes
    end
  
    # POST /tokos
    def create
      @toko = Toko.new(toko_params)
  
      if @toko.save
        render json: @toko.new_attributes, status: :created
      else
        render json: @toko.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /tokos/1
    def update
      if @toko.update(toko_params)
        render json: @toko.new_attributes
      else
        render json: @toko.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /tokos/1
    def destroy
      @toko.destroy
    end
  
    private
  
      # Only allow a trusted parameter "white list" through.
      def toko_params
        params.require(:toko).permit(:nama_toko, :no_telp, :alamat_toko)
      end
  end
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_toko
        @toko = Toko.find_by_id(params[:id])
        if @toko.nil?
          render json: { error: "Toko not found" }, status: :not_found
        end
        def user_params
            {
              nama_toko: params[:nama_toko],
              alamat_toko: params[:alamat_toko],
              no_telp: params[:no_telp],
            }
          end
end
