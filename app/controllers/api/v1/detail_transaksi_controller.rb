class Api::V1::DetailTransaksiController < ApplicationController
    # before_action :set_transaksi, only: [:show, :update, :destroy]

    # GET /transaksi
    def index
      @detailtransaksi = DetailTransaksi.all
  
      render json: @detailtransaksi
    end
  
    # GET /transaksi/1
    def show
      render json: @detailtransaksi.new_attributes
    end
  
    # POST /transaksi
    def create
      @detailtransaksi = DetailTransaksi.new(detailtransaksi_params)
  
      if @detailtransaksi.save
        render json: @detailtransaksi, status: :created
      else
        render json: @detailtransaksi.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /transaksi/1
    def update
      if @detailtransaksi.update(detailtransaksi_params)
        render json: @detailtransaksi.new_attributes
      else
        render json: @detailtransaksi.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /trannsaksi/1
    def destroy
      @detailtransaksi.destroy
    end
  
    private
  
    def set_detailtransaksi
      @detailtransaksi = DetailTransaksi.find_by_id(params[:id])
      if @detailtransaksi.nil?
        render json: { error: "DetailTransaksi not found" }, status: :not_found
      end
    end
    def detailtransaksi_params
    {
      id_detail: params[ :id_detail],
      id_barang: params[ :id_barang],
      nama_barang: params[:nama_barang],
      harga_barang: params[:harga_barang],
      tgl_transaksi: params[:tgl_transaksi],
      total_barang: params[:total_barang],
      total_harga: params[:total_harga]
    }
  end
end
