class Api::V1::ProdukController < ApplicationController
    before_action :set_produk, only: [:show, :update, :destroy]

    # GET /produk
    def index
      @produk= Produk.all
  
      render json: @produk.map { |produk| produk.new_attributes }
    end
  
    # GET /produk/1
    def show
      render json: @produk.new_attributes
    end
  
    # POST /produk
    def create
      @produk = Produk.new(produk_params)
  
      if @produk.save
        render json: @produk.new_attributes, status: :created
      else
        render json: @produk.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /produk/1
    def update
      if @produk.update(produk_params)
        render json: @produk.new_attributes
      else
        render json: @produk_params.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /produk/1
    def destroy
      @produk.destroy
    end
  
    private
  
      # Only allow a trusted parameter "white list" through.
      def produk_params
        params.require(:produk).permit(:id_barang, :nama_barang, :kategori, :harga_beli, :harga_jual)
      end
  end
      def set_produk
        @produk = Produk.find_by_id(params[:id])
        if @produk.nil?
          render json: { error: "Produk not found" }, status: :not_found
        end
        def user_params
            {
              id_barang: params[:id_barang],
              nama_barang: params[:nama_barang],
              harga_beli: params[:harga_beli],
              kategori: params[:kategori],
              harga_jual: params[:harga_jual],
            }
          end
end
