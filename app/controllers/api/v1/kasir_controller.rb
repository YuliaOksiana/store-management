class Api::V1::KasirController < ApplicationController
    before_action :set_kasir, only: [:show, :update, :destroy]

    # GET /kasir
    def index
      @kasir = Kasir.all
  
      render json: @kasir.map { |kasir| kasir.new_attributes }
    end
  
    # GET /kasir/1
    def show
      render json: @kasir.new_attributes
    end
  
    # POST /kasir
    def create
      @kasir = Kasir.new(kasir_params)
  
      if @kasir.save
        render json: @kasir.new_attributes, status: :created
      else
        render json: @kasir.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /kasir/1
    def update
      if @kasir.update(kasir_params)
        render json: @kasir.new_attributes
      else
        render json: @kasir.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /kasir/1
    def destroy
      @kasir.destroy
    end
  
    private
  
      # Only allow a trusted parameter "white list" through.
      def kasir_params
        params.require(:kasir).permit(:nama_kasir, :nama_barang, :total_barang, :harga_barang, :total_diskon, :total_harga)
    end
    def set_kasir
      @kasir = Kasir.find_by_id(params[:id])
      if @kasir.nil?
        render json: { error: "Kasir not found" }, status: :not_found
      end
    end
    def kasir_params
    {
      id_kasir: params[ :id_kasir],
      nama_kasir: params[:nama_kasir],
      nama_barang: params[:nama_barang],
      harga_barang: params[ :harga_barang],
      total_barang: params[:total_barang],
      total_diskon: params[:total_diskon],
      total_harga: params[:total_harga],
      id_toko: params[ :id_toko]
    }
  end
    end

