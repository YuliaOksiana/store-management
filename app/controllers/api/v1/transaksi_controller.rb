class Api::V1::TransaksiController < ApplicationController
    before_action :set_transaksi, only: [:show, :update, :destroy]

    # GET /transaksi
    def index
      @transaksi = Transaksi.all
  
      render json: @transaksi.map { |transaksi| transaksi.new_attributes }
    end
  
    # GET /transaksi/1
    def show
      render json: @transaksi.new_attributes
    end
  
    # POST /transaksi
    def create
      @transaksi = Transaksi.new(transaksi_params)
  
      if @transaksi.save
        render json: @transaksi.new_attributes, status: :created
      else
        render json: @transaksi.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /transaksi/1
    def update
      if @transaksi.update(transaksi_params)
        render json: @transaksi.new_attributes
      else
        render json: @transaksi.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /trannsaksi/1
    def destroy
      @transaksi.destroy
    end
  
    private
  
      # Only allow a trusted parameter "white list" through.
      def transaksi_params
        params.require(:transaksi).permit(:id_transaksi, :id_kasir, :tgl_transaksi, :total_barang, :total_diskon, :total_harga, :metode_bayar)
    end
    def set_transaksi
      @transaksi = Transaksi.find_by_id(params[:id])
      if @transaksi.nil?
        render json: { error: "KaTransaksi not found" }, status: :not_found
      end
    end
    def transaksi_params
    {
      id_transaksi: params[ :id_transaksi],
      id_kasir: params[ :id_kasir],
      tgl_transaksi: params[:tgl_transaksi],
      total_barang: params[:total_barang],
      total_diskon: params[:total_diskon],
      total_harga: params[:total_harga],
      metode_bayar: params[ :metode_bayar],
    }
  end
    end
