class Api::V1::PembeliController < ApplicationController
    before_action :set_pembeli, only: [:show, :update, :destroy]

    # GET /pembeli
    def index
      @pembeli = Pembeli.all
  
      render json: @pembeli.map { |pembeli| pembeli.new_attributes }
    end
  
    # GET /pembeli/1
    def show
      render json: @pembeli.new_attributes
    end
  
    # POST /pembeli
    def create
      @pembeli = Pembeli.new(pembeli_params)
  
      if @pembeli.save
        render json: @pembeli.new_attributes, status: :created
      else
        render json: @pembeli.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /pembeli/1
    def update
      if @pembeli.update(pembeli_params)
        render json: @pembeli.new_attributes
      else
        render json: @pembeli.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /pembeli/1
    def destroy
      @pembeli.destroy
    end
  
    private
  
      # Only allow a trusted parameter "white list" through.
      def pembeli_params
        params.require(:pembeli).permit(:id_pembeli, :tgl_transaksi, :total_barang, :total_diskon, :total_harga, :metode_bayar)
      end
  end
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_pembeli
        @pembeli = Pembeli.find_by_id(params[:id])
        if @pembeli.nil?
          render json: { error: "Pembeli not found" }, status: :not_found
        end
        def user_params
            {
              id_pembeli: params[:id_pembeli],
              tgl_transaksi: params[:tgl_transaksi],
              total_barang: params[:total_barang],
              total_diskon: params[:total_diskon],
              total_harga: params[:total_harga],
              metode_bayar: params[:metode_bayar],
            }
          end
end
