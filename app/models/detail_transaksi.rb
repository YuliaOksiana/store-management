class DetailTransaksi < ApplicationRecord
    
    # validates :id_detail, presence: true, length: { maximum: 50 }
    # validates :id_barang, presence: true, length: { maximum: 500 }
    # validates :nama_barang, presence: true, length: { maximum: 50 }
    # validates :harga_barang, presence: true, length: { maximum: 50 }
    # validates :tgl_transaksi, presence: true
    # validates :total_barang,presence: true, length: { maximum: 500 }
    # validates :total_harga, presence: true
    # validates :total_diskon, presence: true, length: { maximum: 50 }
    # validates :metode_bayar, presence: true, length: { maximum: 500 }

    def new_attributes
    {
      id_detail: self.id_detail,
      id_barang: self.id_kasir,
      nama_barang: self.nama_barang,
      harga_barang: self.harga_barang,
      tgl_transaksi: self.tgl_transaksi,
      total_barang: self.total_barang,
      total_harga: self.total_harga,
      created_at: self.created_at
    }
  end
end
