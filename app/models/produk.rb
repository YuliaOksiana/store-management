class Produk < ApplicationRecord
    validates :id_barang, presence: true, length: { maximum: 50 }
    validates :nama_barang, presence: true, length: { maximum: 500 }
    validates :kategori, presence: true
    validates :harga_beli, presence: true, length: { maximum: 50 }
    validates :harga_jual,presence: true
    def new_attributes
        {
          id_barang: self.id_barang,
          nama_barang: self.nama_barang,
          kategori: self.kategori,
          harga_beli: self.harga_beli,
          harga_jual: self.harga_jual,
          created_at: self.created_at,
        }
      end
end
