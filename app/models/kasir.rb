class Kasir < ApplicationRecord
    validates :id_kasir, presence: true, length: { maximum: 50 }
    validates :nama_kasir, presence: true, length: { maximum: 500 }
    validates :nama_barang, presence: true
    validates :harga_barang, presence: true, length: { maximum: 50 }
    validates :total_barang,presence: true, length: { maximum: 500 }
    validates :total_harga, presence: true
    validates :total_diskon, presence: true, length: { maximum: 50 }
    validates :id_toko, presence: true, length: { maximum: 500 }

    def new_attributes
    {
      id_kasir: self.id_kasir,
      nama_kasir: self.nama_kasir,
      nama_barang: self.nama_barang,
      harga_barang: self.harga_barang,
      total_diskon: self.total_diskon,
      total_harga: self.total_harga,
      id_toko: self.id_toko,
      created_at: self.created_at,
    }
  end
end
