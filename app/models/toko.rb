class Toko < ApplicationRecord
    # validates :id_toko, presence: true, length: { maximum: 50 }
    validates :nama_toko, presence: true, length: { maximum: 500 }
    validates :no_telp, presence: true
    validates :alamat_toko, presence: true, length: { maximum: 50 }
    # validates :id_barang,presence: true

    def new_attributes
        {
          id_toko: self.id,
          nama_toko: self.nama_toko,
          no_telp: self.no_telp,
          alamat: self.alamat_toko,
          # id_barang: self.id_barang,
          created_at: self.created_at,
        }
      end


end
