class Transaksi < ApplicationRecord
    validates :id_transaksi, presence: true, length: { maximum: 50 }
    validates :id_kasir, presence: true, length: { maximum: 500 }
    validates :tgl_transaksi, presence: true
    validates :total_barang,presence: true, length: { maximum: 500 }
    validates :total_harga, presence: true
    validates :total_diskon, presence: true, length: { maximum: 50 }
    validates :metode_bayar, presence: true, length: { maximum: 500 }

    def new_attributes
    {
      id_transaksi: self.id_transaksi,
      id_kasir: self.id_kasir,
      tgl_transaksi: self.tgl_transaksi,
      total_barang: self.total_barang,
      total_diskon: self.total_diskon,
      total_harga: self.total_harga,
      metode_bayar: self.metode_bayar,      
      created_at: self.created_at,
    }
  end
end
