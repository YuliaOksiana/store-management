class Pembeli < ApplicationRecord

    validates :id_pembeli, presence: true, length: { maximum: 50 }
    validates :tgl_transaksi, presence: true
    validates :total_barang,presence: true, length: { maximum: 500 }
    validates :total_harga, presence: true
    validates :total_diskon, presence: true, length: { maximum: 50 }
    validates :metode_bayar, presence: true, length: { maximum: 500 }

    def new_attributes
        {
          id_pembeli: self.id_pembeli,
          tgl_transaksi: self.tgl_transaksi,
          total_barang: self.total_barang,
          total_harga: self.total_harga,
          total_diskon: self.total_diskon,
          created_at: self.created_at,
        }
      end
end
